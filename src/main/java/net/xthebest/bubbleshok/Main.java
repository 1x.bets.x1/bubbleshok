package net.xthebest.bubbleshok;


import net.xthebest.bubbleshok.events.*;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.util.HashMap;

import static net.xthebest.bubbleshok.events.BreakEvent.addBlocks;

public final class Main extends JavaPlugin implements Listener {
    public static Main i;
    public static Main plugin(){return i;}
    public static boolean gameStatus;
    public static HashMap<Player, Integer> money = new HashMap<Player, Integer>();
    public static HashMap<Player,Boolean> checkBar = new HashMap<Player, Boolean>();
    public static HashMap<Player,Boolean> checkOpenClass = new HashMap<Player, Boolean>();
    public static int status = 0;
    public static boolean statusMat = false;

    //class's
    public static HashMap<Player,Boolean> classFermer = new HashMap<>();
    public static HashMap<Player,Boolean> classKiller = new HashMap<>();
    public static HashMap<Player,Boolean> classWaxter = new HashMap<>();
    public static HashMap<Player,Boolean> classPibak = new HashMap<>();
    public static Team team;
    @Override
    public void onEnable() {
        WorldCreator wc = new WorldCreator("wor");
        wc.type(WorldType.NORMAL);
        wc.generateStructures(false);
        wc.createWorld();
        i=this;
        Bukkit.getServer().setWhitelist(true);
//        Bukkit.getScheduler().runTaskLater(this, new Runnable() {
//            @Override
//            public void run() {
//                Bukkit.getServer().setWhitelist(false);
//            }
//        },20*60);
        gameStatus = false;
        team = Bukkit.getScoreboardManager().getNewScoreboard().registerNewTeam("team");
        team.setAllowFriendlyFire(false);
        getCommand("game").setExecutor(new Commands());
        Bukkit.getWorld("world").getWorldBorder().setSize(140);
        getServer().getPluginManager().registerEvents(new BreakEvent(), this);
        getServer().getPluginManager().registerEvents(this, this);
        getServer().getPluginManager().registerEvents(new InteractEvent(), this);
        getServer().getPluginManager().registerEvents(new MoneyEvent(), this);
        addBlocks();
        for(Team t : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()){
            if(t != null){
            t.unregister();
            }
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    if (checkBar.get(player) == null || !checkBar.get(player)) {
                        if(!statusMat){
                            player.teleport(Bukkit.getWorld("wor").getSpawnLocation());
                            Bukkit.getScheduler().runTaskLater(plugin(), new Runnable() {
                                @Override
                                public void run() {
                                    player.teleport(new Location(Bukkit.getWorld("world"),-269, 129, -863));
                                    statusMat = true;
                                }
                            },100);
                        }
                        checkBar.put(player, true);
                        player.getInventory().clear();
                        BarInfo.create(player);
                        player.setInvulnerable(true);
                        if(statusMat) {
                            player.teleport(new Location(Bukkit.getWorld("world"), -269, 129, -863));
                        }
                        player.setGameMode(GameMode.ADVENTURE);
                        player.setCustomName(ChatColor.GRAY + player.getName());
                    }
                    if(status == 2) {
                        if (classKiller.get(player) != null) {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 9999, 0, true, false));
                        }
                        if (classWaxter.get(player) != null) {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 9999, 0, true, false));
                        }
                        if (classPibak.get(player) != null) {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 9999, 0, true, false));
                        }
                    }
                }
            }
        }.runTaskTimer(this,0,150);
        checkPlayerOnline();
        Bukkit.getServer().setWhitelist(false);

    }

    public static HashMap<Player, Integer> scoreDeath = new HashMap<>();
    @EventHandler
    public void join(PlayerJoinEvent event){
        event.setJoinMessage("Игрок " + ChatColor.GOLD +  event.getPlayer().getName()  +ChatColor.WHITE + " присоединился к игре " + ChatColor.GRAY +"[" + Bukkit.getOnlinePlayers().size() + "/8]");
    }
    @EventHandler
    public void dea(PlayerDeathEvent event){
        if(gameStatus) {
            Player player = event.getPlayer();
            scoreDeath.put(player, scoreDeath.get(player) - 1);
            String clas = "ERROR";
            if(classFermer.get(player) != null){
                clas = ChatColor.YELLOW + "Фермер ";
                player.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE,2));
            }
            if(classKiller.get(player) != null){
                clas = ChatColor.RED + "Охотник ";
                player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE,9999,0,true,false));
            }
            if(classWaxter.get(player) != null){
                clas =ChatColor.GOLD + "Шахтер ";
                player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE,9999,0,true,false));
            }
            if(classPibak.get(player) != null){
                clas =ChatColor.AQUA + "Рыбак ";
                player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION,9999,0,true,false));
            }
            Bukkit.getScoreboardManager().getMainScoreboard().getTeam(player.getName()).setPrefix("" + ChatColor.RED + scoreDeath.get(player) + "❤ " + clas);
            if (scoreDeath.get(player) == 0) {
                player.setGameMode(GameMode.SPECTATOR);
                for (int o = 0; o <= 36; o++) {
                    if (player.getInventory().getItem(o) != null) {
                        player.getLocation().getWorld().dropItem(player.getLocation(), player.getInventory().getItem(o));
                        player.getInventory().setItem(o, new ItemStack(Material.AIR));
                    }
                }
            }
            if (event.getEntity().getKiller() != null) {
                event.setDeathMessage("Игрок " + player.getCustomName() + " был убит игроком " + event.getEntity().getKiller().getCustomName());
            }
        }
    }
    @EventHandler
    public void respawn(PlayerRespawnEvent event){
        if(gameStatus) {
            Player player = event.getPlayer();
            if (classFermer.get(player) != null) {
                player.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE, 1));
            }
        }
    }
    public static int gamess;
    public void checkPlayerOnline(){
        gamess = Bukkit.getScheduler().runTaskTimer(plugin(), () -> {
                    if (Bukkit.getOnlinePlayers().size() == 8) {
                        Bukkit.getScheduler().cancelTask(gamess);
                        Bukkit.getScheduler().runTaskLater(plugin(), new Runnable() {
                            @Override
                            public void run() {
                                Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                    for (Player player : Bukkit.getOnlinePlayers()) {
                                        player.sendTitle("", "10");
                                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                        player.sendMessage(ChatColor.YELLOW + "Игра начнется через 10 секунд");
                                    }
                                    Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                        for (Player player : Bukkit.getOnlinePlayers()) {
                                            player.sendTitle("", "9");
                                            player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                            player.sendMessage(ChatColor.YELLOW + "Игра начнется через 9 секунд");
                                        }
                                        Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                            for (Player player : Bukkit.getOnlinePlayers()) {
                                                player.sendTitle("", "8");
                                                player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                                player.sendMessage(ChatColor.YELLOW + "Игра начнется через 8 секунд");
                                            }
                                            Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                                for (Player player : Bukkit.getOnlinePlayers()) {
                                                    player.sendTitle(ChatColor.GOLD + "QIWI BEST123", "7");
                                                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                                    player.sendMessage(ChatColor.YELLOW + "Игра начнется через 7 секунд");
                                                }
                                                Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                                    for (Player player : Bukkit.getOnlinePlayers()) {
                                                        player.sendTitle(ChatColor.DARK_AQUA + "By X_THEBEST_", "6");
                                                        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                                        player.sendMessage(ChatColor.YELLOW + "Игра начнется через 6 секунд");
                                                    }
                                                    Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                                        for (Player player : Bukkit.getOnlinePlayers()) {
                                                            player.sendTitle(ChatColor.YELLOW + "5", "Приготовтесь к игре!");
                                                            player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                                            player.sendMessage(ChatColor.YELLOW + "Игра начнется через 5 секунд");
                                                        }
                                                        Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                                            for (Player player : Bukkit.getOnlinePlayers()) {
                                                                player.sendTitle(ChatColor.RED + "4", "Приготовтесь к игре!");
                                                                player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                                                                player.sendMessage(ChatColor.YELLOW + "Игра начнется через 4 секунды");
                                                            }
                                                            Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                                                for (Player player : Bukkit.getOnlinePlayers()) {
                                                                    player.sendTitle("路", "");
                                                                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 1, 0);
                                                                    player.sendMessage(ChatColor.GOLD + "Игра начнется через 3 секунд");
                                                                }
                                                                Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                                                    for (Player player : Bukkit.getOnlinePlayers()) {
                                                                        player.sendTitle("系", "");
                                                                        player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 1, 1);
                                                                        player.sendMessage(ChatColor.GOLD + "Игра начнется через 2 секунд");
                                                                    }
                                                                    Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                                                        for (Player player : Bukkit.getOnlinePlayers()) {
                                                                            player.sendTitle("ァ", "");
                                                                            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 1, 2);
                                                                            player.sendMessage(ChatColor.GOLD + "Игра начнется через 1 секунд");
                                                                        }
                                                                        StartGame.start();
                                                                    }, 20);
                                                                }, 20);
                                                            }, 20);
                                                        }, 20);
                                                    }, 20);
                                                }, 20);
                                            }, 20);
                                        }, 20);
                                    }, 20);
                                }, 20);
                            }
                        },100);
                    }
        },0,20).getTaskId();

    }
    @Override
    public void onDisable() {
        for(Player player : Bukkit.getOnlinePlayers()){
            player.teleport(new Location(Bukkit.getWorld("world"),-269, 129, -863));
        }
//        BarInfo.remove();
        BarEvent.removeGame();
        team.unregister();
        for(Entity entity : Bukkit.getWorld("world").getEntities()){
            if(entity instanceof Pig){
                entity.remove();
            }
            if(entity instanceof Zombie){
                entity.remove();
            }
            if(entity instanceof Item){
                entity.remove();
            }
        }
        World delete = Bukkit.getWorld("wor");
        Bukkit.unloadWorld(delete, false);
        File deleteFolder = delete.getWorldFolder();
        deleteWorld(deleteFolder);
    }
    public boolean deleteWorld(File path) {
        if(path.exists()) {
            File files[] = path.listFiles();
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
                    deleteWorld(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return(path.delete());
    }
}
