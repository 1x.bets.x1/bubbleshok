package net.xthebest.bubbleshok.events;

import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Team;

import java.util.Random;

import static net.xthebest.bubbleshok.Main.*;

public class BarEvent {
    public static BossBar bar = Bukkit.createBossBar("Ошибка загрузки информации...", BarColor.PURPLE, BarStyle.SEGMENTED_6);
    private static int scheStats = 0;
    public static void BAR(Player player){
        bar.setTitle(ChatColor.RED + "Осталось до выбора класса");
        bar.addPlayer(player);
        int o = 0;
        int finalO = o;
        o = Bukkit.getScheduler().runTaskTimer(plugin(), new Runnable() {
            double pro = 1.0;
            int score = 100;
            double time = 1.0 / 100;
            @Override
            public void run() {
                if(score <= 4){
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK,1,1);
                }
                if(status == 0) {
                    if (pro <= 0.0) {
                        scheStats++;
                        if(scheStats == Bukkit.getOnlinePlayers().size()) {
                            scheStats = 0;
                            status = 1;
                        }
                        pro = 1.0;
                        time = 1.0 / 310;
                        score = 310;
                        StartGame.GUI(player);
                        return;
                    }
                    bar.setProgress(pro);
                    bar.setTitle(ChatColor.RED + "Выбор нового класса через "+ score);
                    pro = pro - time;
                }
                if(status == 1) {
                    if (pro <= 0.0) {
                        scheStats++;
                        if(scheStats == Bukkit.getOnlinePlayers().size()) {
                            scheStats = 0;
                            status = 2;
                        }
                        player.setGameMode(GameMode.ADVENTURE);
                        pro = 1.0;
                        score = 64;
                        time = 1.0 / 64;
                        Bukkit.getWorld("world").getWorldBorder().setSize(100000);
                        player.teleport(new Location(Bukkit.getWorld("world"),-273, 114, -1202,130,0));
                        player.sendTitle("Время закупки!",ChatColor.GOLD+ "У вас есть 64 секунды, чтобы закупится!");
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_TRADE, 1, 1);

                        player.getInventory().clear();
                        if(classFermer.get(player) == null) {
                            if (money.get(player) > 700 && money.get(player) < 1000) {
                                Random n = new Random();
                                int r = n.nextInt(150);
                                player.sendMessage("Вы получили отрицательный бонус " + ChatColor.RED + "-" + r + ChatColor.WHITE + " монет");
                                money.put(player, money.get(player) - r);
                            }
                            if (money.get(player) > 987) {
                                Random n = new Random();
                                int r = n.nextInt(500);
                                player.sendMessage("Вы получили отрицательный бонус " + ChatColor.RED + "-" + r + ChatColor.WHITE + " монет");
                                money.put(player, money.get(player) - r);
                            }
                        }
                        if(money.get(player) < 300){
                            Random n =  new Random();
                            int r = n.nextInt(150);
                            player.sendMessage("Вы получили бонус " +ChatColor.GREEN + "+" + r + ChatColor.WHITE + " монет");
                            money.put(player,money.get(player)+r);
                        }
                        for(int o = 0; o <= money.get(player); o++){
                            player.getInventory().addItem(new ItemStack(Material.EMERALD));
                        }
                        money.put(player,money.get(player)+1);
                        if(checkOpenClass.get(player) == null){
                            player.kickPlayer("Вы не успели выбрать класс");
                            for(Player player1 : Bukkit.getOnlinePlayers()){
                                player1.sendMessage(ChatColor.RED + "Игрок " + player.getName() + " не успел выбрать класс и был удален из игры!");
                            }
                        }
                        Bukkit.getScheduler().cancelTask(InteractEvent.spawnBy);
                        Bukkit.getScheduler().cancelTask(InteractEvent.giveBy);
                        return;
                    }
                    bar.setProgress(pro);
                    bar.setTitle(ChatColor.RED +"Завершение развития через "+ score);
                    pro = pro - time;
                }
                if(status == 2) {
                    if(score == 5) {
                        player.sendMessage(ChatColor.YELLOW + "Начало PVP через 5 секунд");
                    }
                    if(score == 4) {
                        player.sendMessage(ChatColor.YELLOW + "Начало PVP через 4 секунды");
                    }
                    if(score == 3) {
                        player.sendMessage(ChatColor.YELLOW + "Начало PVP через 3 секунды");
                    }
                    if(score == 2) {
                        player.sendMessage(ChatColor.YELLOW + "Начало PVP через 2 секунды");
                    }
                    if(score == 1) {
                        player.sendMessage(ChatColor.YELLOW + "Начало PVP через 1 секунду");
                    }
                    if(score == 0) {
                        for (PotionEffect effect : player.getActivePotionEffects()) {
                            player.removePotionEffect(effect.getType());
                        }
                        player.sendMessage("");
                        player.setBedSpawnLocation(player.getLocation());
                        player.sendMessage("15 секунд бесмертия, чтобы разбежаться!");
                        String clas = "";
                        scoreDeath.putIfAbsent(player, 3);

                        if(classFermer.get(player) != null){
                            clas = ChatColor.YELLOW + "Фермер ";
                            player.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE,2));
                        }
                        if(classKiller.get(player) != null){
                            clas = ChatColor.RED + "Охотник ";
                            player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE,9999,0,true,false));
                        }
                        if(classWaxter.get(player) != null){
                            clas =ChatColor.GOLD + "Шахтер ";
                            player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE,9999,0,true,false));
                        }
                        if(classPibak.get(player) != null){
                            clas =ChatColor.AQUA + "Рыбак ";
                            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION,9999,0,true,false));
                        }
                        Bukkit.getScoreboardManager().getMainScoreboard().getTeam(player.getName()).setPrefix("" + ChatColor.RED + scoreDeath.get(player)+"❤ " + clas);
                        player.getInventory().addItem(new ItemStack(Material.BREAD, 64));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION,99999, 2, true,false));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE,20*15, 2, true,false));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,20*5, 2, true,false));
                        Bukkit.getScheduler().runTaskLater(plugin(), new Runnable() {
                            @Override
                            public void run() {
                                player.playSound(player.getLocation(),Sound.EVENT_RAID_HORN,1,1);
                                player.sendMessage("Бесмертие закончилось, у вас есть 3 жизни!");
                                player.sendTitle(ChatColor.RED + "PVP ON", "У вас 3 жизни, убейте всех!");
                                team.setAllowFriendlyFire(true);
                                player.setInvulnerable(false);
                                scheStats++;
                                if(scheStats == Bukkit.getOnlinePlayers().size()) {
                                    scheStats = 0;
                                    WinEvent();
                                }
                            }
                        }, 20*15);
                    }
                    if (pro <= 0.0) {
                        scheStats++;
                        if(scheStats == Bukkit.getOnlinePlayers().size()) {
                            scheStats = 0;
                            status = 3;
                        }
                        bar.removeAll();
                        pro = 1.0;
                        player.setGameMode(GameMode.ADVENTURE);
                        score = 1200000000;
                        time = 0;
                        Bukkit.getWorld("world").getWorldBorder().setSize(140);
                        Bukkit.getWorld("world").getWorldBorder().setSize(10, 300);
                        Bukkit.getWorld("world").getWorldBorder().setDamageAmount(10);
                        Bukkit.getWorld("world").getWorldBorder().setDamageBuffer(1);
                        player.teleport(new Location(Bukkit.getWorld("world"),-269, 129, -863));
                        Bukkit.getScheduler().cancelTask(finalO);

                        return;
                    }
                    bar.setProgress(pro);
                    bar.setTitle(ChatColor.RED + "Осталось " + score + " сек до ПВП");
                    pro = pro - time;
                }
                score--;
            }
        }, 0,20).getTaskId();
    }
    public static void removeGame(){bar.removeAll();}
    public static int WinEvent;
    public static void WinEvent(){
        WinEvent = Bukkit.getScheduler().runTaskTimer(plugin(), () -> {
            int scoreSurvival = 0;
            for(Player player : Bukkit.getOnlinePlayers()){
                if(player.getGameMode().equals(GameMode.ADVENTURE)){
                    scoreSurvival++;
                }
            }
            String playerWin = "error";
            String playerWin2 = "error";
            if(scoreSurvival == 1){
                Bukkit.getScheduler().cancelTask(WinEvent);
                for(Player player : Bukkit.getOnlinePlayers()){

                    if(player.getGameMode().equals(GameMode.ADVENTURE)){
                        playerWin = "" + ChatColor.LIGHT_PURPLE+ player.getName() + ChatColor.WHITE;
                        playerWin2 = player.getName();
                    }
                    Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                        player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
                        Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                            player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
                            Bukkit.getScheduler().runTaskLater(plugin(), () -> {
                                player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
                                Bukkit.getScheduler().runTaskLater(plugin(), () -> player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK),20);
                            },20);
                        },20);
                    },20);
                }
                for(Player player : Bukkit.getOnlinePlayers()) {
                    player.sendMessage("Игра завершена");

                    player.sendMessage(ChatColor.WHITE + "Игрок " + playerWin + " победил в этой игре!");
                    player.sendTitle(ChatColor.RED + "GAME OVER", "Игрок " + playerWin + " победил");
                    Bukkit.getPlayer(playerWin2).sendTitle(ChatColor.YELLOW + "ПОБЕДА", "Вы стали последним игроком!");
                    Team t = Bukkit.getScoreboardManager().getMainScoreboard().getTeam(player.getName());
                    t.unregister();
                }
                status++;
            }
        },0,20).getTaskId();
    }
}
