package net.xthebest.bubbleshok.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;

import static net.xthebest.bubbleshok.events.BarInfo.sMoney;
import static net.xthebest.bubbleshok.Main.*;

public class MoneyEvent implements Listener {
    @EventHandler
    public void click(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(event.getMaterial().equals(Material.MUSIC_DISC_WAIT)){
            Inventory inventory = Bukkit.createInventory(player, 9, "Продажа");
            if(classFermer.get(player) != null){
                ItemStack item1 = new ItemStack(Material.CARROT);
                ItemMeta item1Meta = item1.getItemMeta();
                item1Meta.setDisplayName(ChatColor.LIGHT_PURPLE+"Продать Еду"+sMoney);
                item1.setItemMeta(item1Meta);
                inventory.setItem(4,  item1);
            }
            if(classKiller.get(player)!= null){
                ItemStack item1 = new ItemStack(Material.PORKCHOP);
                ItemMeta item1Meta = item1.getItemMeta();
                item1Meta.setDisplayName(ChatColor.LIGHT_PURPLE+"Продать");
                item1.setItemMeta(item1Meta);
                inventory.setItem(4,  item1);

            }
            if(classWaxter.get(player)!= null){
                ItemStack item1 = new ItemStack(Material.COAL);
                ItemMeta item1Meta = item1.getItemMeta();
                item1Meta.setDisplayName(ChatColor.LIGHT_PURPLE+"Продать Руду");
                item1.setItemMeta(item1Meta);
                inventory.setItem(4,  item1);
            }
            if(classPibak.get(player)!= null){
                ItemStack item1 = new ItemStack(Material.FISHING_ROD);
                ItemMeta item1Meta = item1.getItemMeta();
                item1Meta.setDisplayName(ChatColor.LIGHT_PURPLE+"Продажа улова "+sMoney);
                item1.setItemMeta(item1Meta);
                inventory.setItem(4,  item1);
            }
            player.openInventory(inventory);
        }
        if(event.getMaterial().equals(Material.MUSIC_DISC_OTHERSIDE)) {
            Inventory inventory = Bukkit.createInventory(player, 9, "Прокачка");
            if (classFermer.get(player) != null) {
                ItemStack item1 = new ItemStack(Material.POTATO);
                ItemMeta item1Meta = item1.getItemMeta();
                item1Meta.setDisplayName(ChatColor.WHITE + "Купить " +ChatColor.GOLD +"Картошку" + ChatColor.WHITE + " за 20  монет"+ sMoney);
                item1.setItemMeta(item1Meta);
                inventory.setItem(1, item1);

                ItemStack item3343 = new ItemStack(Material.SWEET_BERRIES);
                ItemMeta itemMeta3343 = item3343.getItemMeta();
                itemMeta3343.setDisplayName(ChatColor.WHITE + "Купить " +ChatColor.DARK_PURPLE +"Ягоду" + ChatColor.WHITE + " за 30  монет"+ sMoney);
                item3343.setItemMeta(itemMeta3343);
                inventory.setItem(0, item3343);
            }
            if (classFermer.get(player) != null) {
                if (pibacROT.get(player)) {
                    ItemStack item1 = new ItemStack(Material.POTATO);
                    ItemMeta item1Meta = item1.getItemMeta();
                    item1Meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Прокачать Мотыгу 100" + sMoney);
                    item1.setItemMeta(item1Meta);
                    inventory.setItem(5, item1);
                } else {
                    inventory.setItem(3, new ItemStack(Material.BARRIER));
                }
            }
            if (classWaxter.get(player) != null) {
                if (pibacROT.get(player)) {
                    ItemStack item1 = new ItemStack(Material.NETHERITE_PICKAXE);
                    ItemMeta item1Meta = item1.getItemMeta();
                    item1Meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Прокачать Кирку 100" + sMoney);
                    item1.setItemMeta(item1Meta);
                    inventory.setItem(3, item1);
                } else {
                    inventory.setItem(3, new ItemStack(Material.BARRIER));
                }
            }
            if (classPibak.get(player) != null) {
                if (pibacROT.get(player)) {
                    ItemStack item1 = new ItemStack(Material.FISHING_ROD);
                    ItemMeta item1Meta = item1.getItemMeta();
                    item1Meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Прокачать Удочку 100" + sMoney);
                    item1.setItemMeta(item1Meta);
                    inventory.setItem(3, item1);
                } else {
                    inventory.setItem(3, new ItemStack(Material.BARRIER));
                }
            }
            if (classKiller.get(player) != null) {
                if (pibacROT.get(player)) {
                    ItemStack item1 = new ItemStack(Material.ZOGLIN_SPAWN_EGG);
                    ItemMeta item1Meta = item1.getItemMeta();
                    item1Meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Прокачать Меч 100" + sMoney);
                    item1.setItemMeta(item1Meta);
                    inventory.setItem(3, item1);
                } else {
                    inventory.setItem(3, new ItemStack(Material.BARRIER));
                }
            }
            if (pibacDOHOD.get(player) < 3) {
                ItemStack item3343 = new ItemStack(Material.MUSIC_DISC_WAIT);
                ItemMeta itemMeta3343 = item3343.getItemMeta();
                itemMeta3343.setDisplayName(ChatColor.DARK_AQUA + "Прокачать доход за 150" + sMoney);
                item3343.setItemMeta(itemMeta3343);
                inventory.setItem(5, item3343);
            } else {
                inventory.setItem(5, new ItemStack(Material.BARRIER));
            }
            player.openInventory(inventory);

        }
    }
    public static HashMap<Player, Boolean> pibacROT = new HashMap<Player, Boolean>();

    public static HashMap<Player, Integer> pibacDOHOD = new HashMap<Player, Integer>();
    @EventHandler
    public void kill(EntityDamageByEntityEvent event){
        if(gameStatus) {
            if (event.getDamager() instanceof Player) {

                Player player = (Player) event.getDamager();
                if(classKiller.get(player) != null) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 133, 1, true, false));
                    if(event.getEntity().equals(EntityType.ZOMBIE) || event.getEntity().equals(EntityType.PIG) || event.getEntity().equals(EntityType.COW) || event.getEntity().equals(EntityType.CREEPER) || event.getEntity().equals(EntityType.SKELETON)|| event.getEntity().equals(EntityType.CHICKEN)){
                        money.put(player, money.get(player) + 2);
                    }else{
                        money.put(player, money.get(player) + 1);
                    }
                }
            }
        }
    }
    public static HashMap<ItemStack, Integer> pibaHahs = new HashMap<ItemStack, Integer>();
    @EventHandler
    public void pibak(PlayerFishEvent event) {

        Player player = event.getPlayer();
        if (event.getCaught() != null){
            Item i = (Item) event.getCaught();
            pibaHahs.put(i.getItemStack(), i.getItemStack().getAmount());
            money.put(player, money.get(player) + 10);
        }
    }
}
