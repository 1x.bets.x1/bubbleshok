package net.xthebest.bubbleshok.events;

import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Team;

import java.util.Map;
import java.util.Objects;
import java.util.Random;

import static net.xthebest.bubbleshok.events.BarInfo.sMoney;
import static net.xthebest.bubbleshok.Main.*;

public class InteractEvent implements Listener {
    public static int giveBy;
    public static int spawnBy;

    @EventHandler
    public void inter(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getCurrentItem() == null) {
            return;
        }
        String stringPlayer = ChatColor.GOLD + player.getName() + ChatColor.WHITE;
        if(event.getCurrentItem().getItemMeta() != null) {
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "Прокачать класс") ||
                    event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GREEN + "Продать")) {
                event.setCancelled(true);
            }
            String clas = "";
            if (event.getView().getTitle().equalsIgnoreCase("Class")) {
                event.setCancelled(true);
                player.getInventory().clear();
                MoneyEvent.pibacDOHOD.put(player, 1);
                MoneyEvent.pibacROT.put(player, true);
                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.YELLOW + "Класс Фермер")) {
                    player.closeInventory();
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
                    checkOpenClass.put(player, true);
                    classFermer.put(player, true);

                    player.setCustomName(ChatColor.YELLOW + "Фермер " + ChatColor.WHITE + player.getName());
                    clas = ChatColor.YELLOW + "Фермер ";
                    player.sendTitle("", "Вы выбрали класс " + ChatColor.YELLOW + "Фермер");
                    player.getInventory().setItem(0, new ItemStack(Material.WOODEN_HOE));
                    player.getInventory().setItem(1, new ItemStack(Material.BONE_MEAL));
                    player.getInventory().setItem(3, new ItemStack(Material.CARROT));

                    ItemStack itemStack = new ItemStack(Material.MUSIC_DISC_OTHERSIDE);
                    ItemMeta iM = itemStack.getItemMeta();
                    iM.setDisplayName(ChatColor.LIGHT_PURPLE + "Прокачать класс");
                    itemStack.setItemMeta(iM);
                    player.getInventory().setItem(8, itemStack);

                    ItemStack prodaja = new ItemStack(Material.MUSIC_DISC_WAIT);
                    ItemMeta iM2 = prodaja.getItemMeta();
                    iM2.setDisplayName(ChatColor.GREEN + "Продать");
                    prodaja.setItemMeta(iM2);
                    player.getInventory().setItem(4, prodaja);

                    for (Player player1 : Bukkit.getOnlinePlayers()) {
                        player1.sendMessage("Игрок " + stringPlayer + " выбрал класс " + ChatColor.YELLOW + "Фермер");
                    }
                    player.getWorld().setGameRule(GameRule.RANDOM_TICK_SPEED, 35);

                    giveBy = Bukkit.getScheduler().runTaskTimer(plugin(), new Runnable() {
                        @Override
                        public void run() {
                            if(status == 1) {
                                player.getInventory().addItem(new ItemStack(Material.BONE_MEAL, 2));
                                player.getInventory().addItem(new ItemStack(Material.CARROT));
                            }
                        }
                    }, 0, 180).getTaskId();
                }
                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.RED + "Класс Убийца")) {
                    player.closeInventory();
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
                    checkOpenClass.put(player, true);
                    classKiller.put(player, true);
                    clas = ChatColor.RED + "Охотник ";

                    player.setCustomName(ChatColor.RED + "Охотник " + ChatColor.WHITE + player.getName());
                    ItemStack itemStack = new ItemStack(Material.MUSIC_DISC_OTHERSIDE);
                    ItemMeta iM = itemStack.getItemMeta();
                    iM.setDisplayName(ChatColor.LIGHT_PURPLE + "Прокачать класс");
                    itemStack.setItemMeta(iM);
                    player.getInventory().setItem(8, itemStack);

                    ItemStack prodaja = new ItemStack(Material.MUSIC_DISC_WAIT);
                    ItemMeta iM2 = prodaja.getItemMeta();
                    iM2.setDisplayName(ChatColor.GREEN + "Продать");
                    prodaja.setItemMeta(iM2);
                    player.getInventory().setItem(4, prodaja);

                    player.sendTitle("", "Вы выбрали класс " + ChatColor.RED + "Охотник");
                    for (Player player1 : Bukkit.getOnlinePlayers()) {
                        player1.sendMessage("Игрок " + stringPlayer + " выбрал класс " + ChatColor.RED + "Охотник");
                    }
                    ItemStack i = new ItemStack(Material.WOODEN_SWORD);
                    ItemMeta itemMeta = i.getItemMeta();
                    itemMeta.setUnbreakable(true);
                    i.setItemMeta(itemMeta);
                    player.getInventory().setItem(0, i);
                    spawnBy = Bukkit.getScheduler().runTaskTimer(plugin(), new Runnable() {
                        @Override
                        public void run() {
                            if(status == 1) {
                                Random randomx = new Random();
                                Random randomz = new Random();
                                int x = randomx.nextInt(15);
                                int z = randomz.nextInt(15);

                                Random zoRando = new Random();
                                int randomSpawn = zoRando.nextInt(2);
                                Entity pig = null;
                                if (randomSpawn == 0) {
                                    pig = Bukkit.getWorld(player.getWorld().getName()).spawnEntity(new Location(player.getWorld(), (player.getLocation().getX() - 4) + x,
                                            player.getLocation().getY(), (player.getLocation().getZ() - 8)), EntityType.PIG);
                                    Pig p = (Pig) pig;
                                    p.setTarget(player);
                                }
                                if (randomSpawn == 1) {
                                    pig = Bukkit.getWorld(player.getWorld().getName()).spawnEntity(new Location(player.getWorld(), (player.getLocation().getX() - 4) + x,
                                            player.getLocation().getY(), (player.getLocation().getZ() - 8)), EntityType.ZOMBIE);
                                    Zombie zombie = (Zombie) pig;
                                    zombie.setTarget(player);
                                    zombie.getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET));
                                }
                                if (pig.getLocation().getBlock().getType() != Material.AIR) {
                                    pig.teleport(new Location(pig.getWorld(), pig.getLocation().getBlockX(),
                                            pig.getLocation().getBlockY() + 1, pig.getLocation().getBlockZ()));
                                    for (int is = 0; is <= 5; is++) {
                                        if (pig.getLocation().getBlock().getType() != Material.AIR) {
                                            pig.teleport(new Location(pig.getWorld(), pig.getLocation().getBlockX(),
                                                    pig.getLocation().getBlockY() + 1, pig.getLocation().getBlockZ()));
                                        }
                                    }
                                }
                            }
                        }
                    }, 0, 125).getTaskId();
                }
                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.WHITE + "Класс ШАХТЕР")) {
                    player.closeInventory();
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
                    checkOpenClass.put(player, true);
                    classWaxter.put(player, true);
                    clas =ChatColor.GOLD + "Шахтер ";

                    player.setCustomName(ChatColor.GOLD + "Шахтер " + ChatColor.WHITE + player.getName());

                    ItemStack itemStack = new ItemStack(Material.MUSIC_DISC_OTHERSIDE);
                    ItemMeta iM = itemStack.getItemMeta();
                    iM.setDisplayName(ChatColor.LIGHT_PURPLE + "Прокачать класс");
                    itemStack.setItemMeta(iM);
                    player.getInventory().setItem(8, itemStack);

                    ItemStack prodaja = new ItemStack(Material.MUSIC_DISC_WAIT);
                    ItemMeta iM2 = prodaja.getItemMeta();
                    iM2.setDisplayName(ChatColor.GREEN + "Продать");
                    prodaja.setItemMeta(iM2);
                    player.getInventory().setItem(4, prodaja);

                    player.sendTitle("", "Вы выбрали класс " + ChatColor.GRAY + "ШАХТЕР");
                    ItemStack i = new ItemStack(Material.NETHERITE_PICKAXE);
                    ItemMeta itemMeta = i.getItemMeta();
                    itemMeta.addEnchant(Enchantment.DIG_SPEED, 5, true);
                    itemMeta.setUnbreakable(true);
                    i.setItemMeta(itemMeta);
                    player.getInventory().setItem(0, i);
                    for (Player player1 : Bukkit.getOnlinePlayers()) {
                        player1.sendMessage("Игрок " + stringPlayer + " выбрал класс " + ChatColor.GOLD + "Шахтер");
                    }

                }
                if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.AQUA + "Класс Рыбак")) {
                    player.closeInventory();
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 2);
                    checkOpenClass.put(player, true);
                    classPibak.put(player, true);
                    clas =ChatColor.AQUA + "Рыбак ";

                    player.setCustomName(ChatColor.AQUA + "Рыбак " + ChatColor.WHITE + player.getName());

                    ItemStack itemStack = new ItemStack(Material.MUSIC_DISC_OTHERSIDE);
                    ItemMeta iM = itemStack.getItemMeta();
                    iM.setDisplayName(ChatColor.LIGHT_PURPLE + "Прокачать класс");
                    itemStack.setItemMeta(iM);
                    player.getInventory().setItem(8, itemStack);

                    ItemStack prodaja = new ItemStack(Material.MUSIC_DISC_WAIT);
                    ItemMeta iM2 = prodaja.getItemMeta();
                    iM2.setDisplayName(ChatColor.GREEN + "Продать");
                    prodaja.setItemMeta(iM2);
                    player.getInventory().setItem(4, prodaja);

                    player.sendTitle("", "Вы выбрали класс " + ChatColor.AQUA + "Рыбак");
                    ItemStack i = new ItemStack(Material.FISHING_ROD);
                    ItemMeta itemMeta = i.getItemMeta();
                    itemMeta.addEnchant(Enchantment.LURE, 1, true);
                    itemMeta.setUnbreakable(true);
                    i.setItemMeta(itemMeta);
                    player.getInventory().setItem(0, i);
                    MoneyEvent.pibacDOHOD.put(player, 1);
                    for (Player player1 : Bukkit.getOnlinePlayers()) {
                        player1.sendMessage("Игрок " + stringPlayer + " выбрал класс " + ChatColor.AQUA + "Рыбак");
                    }
                }
            }
            player.setPlayerListName(player.getCustomName());
            if(player.getName().equalsIgnoreCase("X_THEBEST_")){
                player.setPlayerListName("ル "+player.getCustomName());
            }
                if(Bukkit.getScoreboardManager().getMainScoreboard().getTeam(player.getName()) == null){
                Team t = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam(player.getName());
                t.setPrefix(clas);
                t.addPlayer(player);
            }else{
                Bukkit.getScoreboardManager().getMainScoreboard().getTeam(player.getName()).unregister();
                Team t = Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam(player.getName());
                t.setPrefix(clas);
                t.addPlayer(player);
            }

        }
        if (event.getView().getTitle().equalsIgnoreCase("Прокачка")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE+"Прокачать Удочку 100"+sMoney)) {
                if(money.get(player) >= 100) {
                    for (int o = 0; o <= 36; o++) {
                        if (player.getInventory().getItem(o) != null) {
                            Material material = player.getInventory().getItem(o).getType();
                            if (material == Material.FISHING_ROD) {
                                player.getInventory().setItem(0, new ItemStack(Material.AIR));
                            }
                        }
                    }
                    ItemStack i = new ItemStack(Material.FISHING_ROD);
                    ItemMeta itemMeta = i.getItemMeta();
                    itemMeta.addEnchant(Enchantment.LURE, 3, true);
                    itemMeta.addEnchant(Enchantment.LUCK, 2, true);
                    itemMeta.setUnbreakable(true);
                    i.setItemMeta(itemMeta);
                    player.getInventory().setItem(0, i);
                    player.getInventory().addItem(new ItemStack(Material.WOODEN_SWORD));
                    money.put(player, money.get(player) - 100);
                    player.sendTitle("","阪");

                    MoneyEvent.pibacROT.put(player, false);
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,1,1);
                }else{
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO,1,1);
                    player.sendMessage(ChatColor.RED + "Недостаточно монет для совершения покупки");
                }
            }

            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE+"Прокачать Кирку 100"+sMoney)) {
                if(money.get(player) >= 100) {
                    for (int o = 0; o <= 36; o++) {
                        if (player.getInventory().getItem(o) != null) {
                            Material material = player.getInventory().getItem(o).getType();
                            if (material == Material.NETHERITE_PICKAXE) {
                                player.getInventory().setItem(0, new ItemStack(Material.AIR));
                            }
                        }
                    }
                    ItemStack i = new ItemStack(Material.NETHERITE_PICKAXE);
                    ItemMeta itemMeta = i.getItemMeta();
                    itemMeta.addEnchant(Enchantment.DIG_SPEED, 5, true);
                    itemMeta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 1, true);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING,99999,4,true,false));
                    itemMeta.setUnbreakable(true);
                    i.setItemMeta(itemMeta);
                    player.getInventory().setItem(0, i);
                    money.put(player, money.get(player) - 100);
                    player.sendTitle("","阪");

                    MoneyEvent.pibacROT.put(player, false);
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,1,1);
                }else{
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO,1,1);
                    player.sendMessage(ChatColor.RED + "Недостаточно монет для совершения покупки");
                }
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "Прокачать Мотыгу 100" + sMoney)) {
                if(money.get(player) >= 100) {
                    for (int o = 0; o <= 36; o++) {
                        if (player.getInventory().getItem(o) != null) {
                            Material material = player.getInventory().getItem(o).getType();
                            if (material == Material.WOODEN_HOE) {
                                player.getInventory().setItem(0, new ItemStack(Material.AIR));
                            }
                        }
                    }
                    ItemStack i = new ItemStack(Material.NETHERITE_HOE);
                    ItemMeta itemMeta = i.getItemMeta();
                    itemMeta.addEnchant(Enchantment.LUCK, 4, true);
                    itemMeta.setUnbreakable(true);
                    i.setItemMeta(itemMeta);
                    player.getInventory().setItem(0, i);
                    money.put(player, money.get(player) - 100);
                    player.sendTitle("","阪");
                    MoneyEvent.pibacROT.put(player, false);
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,1,1);
                }else{
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO,1,1);
                    player.sendMessage(ChatColor.RED + "Недостаточно монет для совершения покупки");
                }
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "Прокачать Меч 100" + sMoney)) {
                if(money.get(player) >= 100) {
                    for (int o = 0; o <= 36; o++) {
                        if (player.getInventory().getItem(o) != null) {
                            Material material = player.getInventory().getItem(o).getType();
                            if (material == Material.WOODEN_SWORD) {
                                player.getInventory().setItem(0, new ItemStack(Material.AIR));
                            }
                        }
                    }
                    ItemStack i = new ItemStack(Material.WOODEN_SWORD);
                    ItemMeta itemMeta = i.getItemMeta();
                    itemMeta.addEnchant(Enchantment.LOOT_BONUS_MOBS, 4, true);
                    itemMeta.setUnbreakable(true);
                    i.setItemMeta(itemMeta);
                    player.getInventory().setItem(0, i);
                    money.put(player, money.get(player) - 100);
                    player.sendTitle("","阪");
                    MoneyEvent.pibacROT.put(player, false);
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,1,1);
                }else{
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO,1,1);
                    player.sendMessage(ChatColor.RED + "Недостаточно монет для совершения покупки");
                }
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.WHITE + "Купить " +ChatColor.GOLD +"Картошку" + ChatColor.WHITE + " за 20  монет"+ sMoney)) {
                if(money.get(player) >= 20) {
                    player.getInventory().addItem(new ItemStack(Material.POTATO));
                    money.put(player, money.get(player) - 20);
                    player.sendTitle("","阪");
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,1,1);
                }else{
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO,1,1);
                    player.sendMessage(ChatColor.RED + "Недостаточно монет для совершения покупки");
                }
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.WHITE + "Купить " +ChatColor.DARK_PURPLE +"Ягоду" + ChatColor.WHITE + " за 30  монет"+ sMoney)) {
                if(money.get(player) >= 30) {
                    player.getInventory().addItem( new ItemStack(Material.SWEET_BERRIES));
                    money.put(player, money.get(player) - 30);
                    player.sendTitle("","阪");
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,1,1);
                }else{
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO,1,1);
                    player.sendMessage(ChatColor.RED + "Недостаточно монет для совершения покупки");
                }
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_AQUA + "Прокачать доход за 150" + sMoney)) {
                if(money.get(player) >= 150) {
                    money.put(player, money.get(player) - 150);
                    MoneyEvent.pibacDOHOD.put(player, MoneyEvent.pibacDOHOD.get(player) + 1);
                    player.sendTitle("","阪");
                    player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,1,1);

                }else{
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO,1,1);
                    player.sendMessage(ChatColor.RED + "Недостаточно монет для совершения покупки");
                }
            }
            player.closeInventory();

        }
        if (event.getView().getTitle().equalsIgnoreCase("Продажа")) {
            event.setCancelled(true);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_CELEBRATE, 1, 1);
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "Продажа улова " + sMoney)) {
                int finalMoney = 0;
                for (Map.Entry<ItemStack, Integer> entry : MoneyEvent.pibaHahs.entrySet()) {
                    for (int o = 0; o <= 40; o++) {
                        if (player.getInventory().getItem(o) != null) {
                            Material material = player.getInventory().getItem(o).getType();
                            if (material.equals(Material.COD) ||
                                    material.equals(Material.SALMON) || material.equals(Material.PUFFERFISH) || material.equals(Material.TROPICAL_FISH) || material.equals(Material.NAME_TAG) || material.equals(Material.NAUTILUS_SHELL)) {
                                if (material.equals(Material.COD)) {
                                    int mo = player.getInventory().getItem(o).getAmount() * 10;
                                    finalMoney = finalMoney + mo;
                                    player.sendMessage("+" + mo + sMoney + " за " + ChatColor.GREEN + "x" + player.getInventory().getItem(o).getAmount() + " " + net.md_5.bungee.api.ChatColor.of("#CC9C74") + "Сырую треску");
                                }
                                if (material.equals(Material.SALMON)) {
                                    int mo = player.getInventory().getItem(o).getAmount() * 15;
                                    finalMoney = finalMoney + mo;
                                    player.sendMessage("+" + mo + sMoney + " за " + ChatColor.GREEN + "x" + player.getInventory().getItem(o).getAmount() + " " + net.md_5.bungee.api.ChatColor.of("#CC9C74") + "Сырой лосось");
                                }
                                if (material.equals(Material.PUFFERFISH)) {
                                    int mo = player.getInventory().getItem(o).getAmount() * 45;
                                    finalMoney = finalMoney + mo;
                                    player.sendMessage("+" + mo + sMoney + " за " + ChatColor.GREEN + "x" + player.getInventory().getItem(o).getAmount() + " " + net.md_5.bungee.api.ChatColor.of("#E55137") + "Рыбу-Фугу");
                                }
                                if (material.equals(Material.TROPICAL_FISH)) {
                                    int mo = player.getInventory().getItem(o).getAmount() * 35;
                                    finalMoney = finalMoney + mo;
                                    player.sendMessage("+" + mo + sMoney + " за " + ChatColor.GREEN + "x" + player.getInventory().getItem(o).getAmount() + " " + net.md_5.bungee.api.ChatColor.of("#EEDC82") + "Тропоческая рыба");
                                }
                                if (material.equals(Material.NAME_TAG)) {
                                    int mo = player.getInventory().getItem(o).getAmount() * 55;
                                    finalMoney = finalMoney + mo;
                                    player.sendMessage("+" + mo + sMoney + " за " + ChatColor.GREEN + "x" + player.getInventory().getItem(o).getAmount() + " " + net.md_5.bungee.api.ChatColor.of("#AACBE6") + "Бирку");
                                }
                                if (material.equals(Material.NAUTILUS_SHELL)) {
                                    int mo = player.getInventory().getItem(o).getAmount() * 55;
                                    finalMoney = finalMoney + mo;
                                    player.sendMessage("+" + mo + sMoney + " за " + ChatColor.GREEN + "x" + player.getInventory().getItem(o).getAmount() + " " + net.md_5.bungee.api.ChatColor.of("#DE5D83") + "Ракушку");
                                }
                                player.getInventory().setItem(o, new ItemStack(Material.AIR));
                            } else {
                                if (Objects.equals(player.getInventory().getItem(o), entry.getKey())) {
                                    int mo = player.getInventory().getItem(o).getAmount() * 2;
                                    player.sendMessage("Мусор " + ChatColor.RED + "x" + player.getInventory().getItem(o).getAmount() + " " + ChatColor.GOLD + mo + sMoney);
                                    finalMoney = finalMoney + mo;
                                    player.getInventory().setItem(o, new ItemStack(Material.AIR));
                                }
                            }
                        }else{
                            player.sendMessage(ChatColor.RED + "Вы должны что-то поймать, чтобы продать!");
                        }
                    }
                }
                MoneyEvent.pibaHahs.clear();
                player.sendMessage("Вы сумарно заработали " + ChatColor.GOLD + "x" + MoneyEvent.pibacDOHOD.get(player) + " " + (finalMoney * MoneyEvent.pibacDOHOD.get(player)) + sMoney);
                money.put(player, money.get(player) + (finalMoney * MoneyEvent.pibacDOHOD.get(player)));
                player.closeInventory();

            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE+"Продать" )) {
                int finalMoney = 0;
                for (int o = 0; o <= 36; o++) {
                    if (player.getInventory().getItem(o) != null) {
                        Material material = player.getInventory().getItem(o).getType();
                        if(material.equals(Material.ROTTEN_FLESH)){
                            int score = 3 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#D6835C") + "Гнелой плоти" + ChatColor.WHITE+" вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                        if(material.equals(Material.GUNPOWDER)){
                            int score = player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.GRAY + "Пороха" + ChatColor.WHITE+" вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                        if(material.equals(Material.IRON_INGOT)){
                            int score = player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#D6835C") + "Железa" + ChatColor.WHITE+" вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.PORKCHOP)){
                            int score = player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#CC8899") + "Сырой свинины" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.MUTTON)){
                            int score = 2 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#CC8899") + "Сырой баранины" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.RABBIT)){
                            int score = 5 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#FCB8CD") + "Сырой крольчатины" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.CHICKEN)){
                            int score = 3 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#CC8899") + "Курятины" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.BEEF)){
                            int score = 2 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#FF5C77") + "Сырой говядины" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                        if(material.equals(Material.IRON_HELMET)){
                            int score = 10 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#FF5C77") + "Железной каски" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                        if(material.equals(Material.CARROT)){
                            int score = 10 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#FF5C77") + "Моркови" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                        if(material.equals(Material.POTATO)){
                            int score = 10 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#FF5C77") + "Картошки" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                    }
                }
                player.sendMessage("Вы сумарно заработали " + ChatColor.GOLD + "x" + MoneyEvent.pibacDOHOD.get(player) + " " + (finalMoney * MoneyEvent.pibacDOHOD.get(player)) + sMoney);
                money.put(player, money.get(player) + (finalMoney * MoneyEvent.pibacDOHOD.get(player)));
                player.closeInventory();
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE+"Продать Руду")) {
                int finalMoney = 0;
                for (int o = 0; o <= 36; o++) {
                    if (player.getInventory().getItem(o) != null) {
                        Material material = player.getInventory().getItem(o).getType();
                        if(material.equals(Material.COAL)){
                            int score = player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#332F2C") + "Угля" + ChatColor.WHITE+" вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                        if(material.equals(Material.RAW_IRON)){
                            int score = player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.DARK_GRAY + "Железа" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.RAW_GOLD)){
                            int score = 2 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.GOLD + "Золота" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.LAPIS_LAZULI)){
                            int score = player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.DARK_BLUE + "Лазурита" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                        if(material.equals(Material.DIAMOND)){
                            int score = 10 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.AQUA + "Алмаза" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.EMERALD)){
                            int score = 25 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.DARK_GREEN + "Изумруда" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                    }
                }
                player.sendMessage("Вы сумарно заработали " + ChatColor.GOLD + "x" + MoneyEvent.pibacDOHOD.get(player) + " " + (finalMoney * MoneyEvent.pibacDOHOD.get(player)) + sMoney);
                money.put(player, money.get(player) + (finalMoney * MoneyEvent.pibacDOHOD.get(player)));
                player.closeInventory();
            }
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE+"Продать Еду" + sMoney)) {
                int finalMoney = 0;
                for (int o = 0; o <= 36; o++) {
                    if (player.getInventory().getItem(o) != null) {
                        Material material = player.getInventory().getItem(o).getType();
                        if(material.equals(Material.CARROT)){
                            int score = player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ net.md_5.bungee.api.ChatColor.of("#332F2C") + "Морковки" + ChatColor.WHITE+" вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.APPLE)){
                            int score = 3 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.DARK_GRAY + "Яблока" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));

                        }
                        if(material.equals(Material.POTATO)){
                            int score = 3 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.GOLD + "Картошки" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                        if(material.equals(Material.SWEET_BERRIES)){
                            int score = 2 * player.getInventory().getItem(o).getAmount();
                            finalMoney = finalMoney + score;
                            player.sendMessage("За продажу "+ ChatColor.GOLD + "Ягод" + ChatColor.WHITE+ " вы заработали " + score+ sMoney);
                            player.getInventory().setItem(o, new ItemStack(Material.AIR));
                        }
                    }
                }
                player.sendMessage("Вы сумарно заработали " + ChatColor.GOLD + "x" + MoneyEvent.pibacDOHOD.get(player) + " " + (finalMoney * MoneyEvent.pibacDOHOD.get(player)) + sMoney);
                money.put(player, money.get(player) + (finalMoney * MoneyEvent.pibacDOHOD.get(player)));
                player.closeInventory();
            }
        }
    }
    @EventHandler
    public void drop(PlayerDropItemEvent event){
        event.setCancelled(true);
    }
}
