package net.xthebest.bubbleshok.events;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import static net.xthebest.bubbleshok.events.StartGame.start;
import static net.xthebest.bubbleshok.Main.money;

public class Commands implements CommandExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
//        Player player = (Player) sender;
//        start();
//        for(Player player1 : Bukkit.getOnlinePlayers()){
//            player1.sendMessage("Игрок " +player.getName() +" принудительно запустил игру");
//        }
        if(args.length == 2) {
            int i = Integer.parseInt(args[0]);
            Player player = Bukkit.getPlayer(args[1]);
            money.put(player, money.get(player) - i);
        }else {
            Player player = (Player) sender;
            start();
            for (Player player1 : Bukkit.getOnlinePlayers()) {
                player1.sendMessage("Игрок " + player.getName() + " принудительно запустил игру");
            }
        }
        return false;
    }
}
