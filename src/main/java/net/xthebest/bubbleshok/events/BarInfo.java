package net.xthebest.bubbleshok.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Locale;

import static net.xthebest.bubbleshok.Main.*;

public class BarInfo {
    public static String sMoney = ChatColor.WHITE+("牡");
    public static void create(Player player){
        BossBar bar = Bukkit.createBossBar("Ошибка загрузки информации...", BarColor.PINK, BarStyle.SEGMENTED_6);
        money.putIfAbsent(player,0);
        new BukkitRunnable() {
            @Override
            public void run() {
                String iMoney = "" + ChatColor.YELLOW + String.valueOf(money.get(player));
                if (money.get(player) >= 1000) {
                    float m = (money.get(player) / 1000F);
                    m = Float.parseFloat(String.format(Locale.US, "%.1f", m));
                    iMoney = "" + ChatColor.YELLOW + m + " тыс.";
                }
                if (money.get(player) >= 1000000) {
                    float m = money.get(player) / 1000000F;
                    m = Float.parseFloat(String.format(Locale.US, "%.1f", m));
                    iMoney = "" + ChatColor.YELLOW + m + " млн.";
                }
                if (gameStatus) {
                    if (status <= 2) {
                        bar.setTitle(sMoney + "Монеты " + iMoney);
                        String classs = "⨇";
                        if (classWaxter.get(player) != null) {
                            classs = "⨄";
                        }
                        if (classFermer.get(player) != null) {
                            classs = "⨕";
                        }
                        if (classKiller.get(player) != null) {
                            classs = "⨏";
                        }
                        if (classPibak.get(player) != null) {
                            classs = "⨁";
                        }
                        player.sendActionBar("⨀ " + classs);
                    } else {
                        if(status == 3){
                            if(player.getGameMode().equals(GameMode.ADVENTURE)){
                            bar.setTitle(ChatColor.RED + "❤ " + ChatColor.WHITE + "У вас жизней " +ChatColor.RED+scoreDeath.get(player));
                            }else{ bar.removeAll();}
                        }else{ bar.removeAll();}
                    }
                } else {
                    int online = Bukkit.getOnlinePlayers().size();
                    String probel = "                                                                                                                                    ";
                    if (online == 1) {
                        bar.setTitle("ᾗ" + probel);
                    }
                    if (online == 2) {
                        bar.setTitle("ᾟ" + probel);
                    }
                    if (online == 3) {
                        bar.setTitle("ᾑ" + probel);
                    }
                    if (online == 4) {
                        bar.setTitle("ᾙ" + probel);
                    }
                    if (online == 5) {
                        bar.setTitle("ἥ" + probel);
                    }
                    if (online == 6) {
                        bar.setTitle("Ἥ" + probel);
                    }
                    if (online == 7) {
                        bar.setTitle("ὴ" + probel);
                    }
                    if (online == 8) {
                        bar.setTitle("Ἦ" + probel);
                    }
                }
            }

        }.runTaskTimer(plugin(), 0,20);
        bar.addPlayer(player);
    }
//    public static void remove(){bar.removeAll();}
}
