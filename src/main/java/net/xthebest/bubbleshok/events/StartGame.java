package net.xthebest.bubbleshok.events;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

import static net.xthebest.bubbleshok.events.BarEvent.BAR;
import static net.xthebest.bubbleshok.Main.*;

public class StartGame {
    public static void start(){
        Bukkit.getScheduler().runTaskTimer(plugin(), new Runnable() {
            @Override
            public void run() {
                if(Bukkit.getOnlinePlayers().size() == 0){
                    Bukkit.getServer().reload();
                }
            }
        },0,150);

        Bukkit.getServer().setWhitelist(true);
        for(Player player : Bukkit.getOnlinePlayers()) {
            player.setInvulnerable(true);
            player.getInventory().clear();
            player.teleport(Bukkit.getWorld("wor").getSpawnLocation());
            player.setGameMode(GameMode.SURVIVAL);
            gameStatus = true;
            for (PotionEffect effect : player.getActivePotionEffects()) {
                player.removePotionEffect(effect.getType());
            }
            Bukkit.getScheduler().runTaskLater(plugin(), new Runnable() {
                @Override
                public void run() {
                    BAR(player);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 999999,3, false,false));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 999999,3, false,false));
                    player.sendTitle("⨋","",10,10,10);
                    player.playSound(player.getLocation(), Sound.EVENT_RAID_HORN,100,2);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_AMBIENT,1,2);
                            player.sendTitle("","⨊");
                        }
                    }.runTaskLater(plugin(), 30);
                }
            }, 25);
        }
    }
    public static void GUI(Player player){
        Inventory inventory = Bukkit.createInventory(player, 54, "Class");

        ItemStack gui = new ItemStack(Material.MUSIC_DISC_11);
        ItemMeta guiMeta = gui.getItemMeta();
        guiMeta.setDisplayName("By X_THEBEST_");
        gui.setItemMeta(guiMeta);
        inventory.setItem(53, gui);
        //FERMER
        Bukkit.getWorld(player.getWorld().getUID()).setDifficulty(Difficulty.NORMAL);
        ItemStack fermer = new ItemStack(Material.BARRIER);
        ItemMeta fermerMeta = gui.getItemMeta();
        fermerMeta.setDisplayName(ChatColor.YELLOW + "Класс Фермер");
        List<String> fermerLore = new ArrayList<>();
        fermerLore.add("");
        fermerLore.add(ChatColor.GRAY + " Вы прирожденный фермер");
        fermerLore.add(ChatColor.GRAY + " Зарабатывайте монеты");
        fermerLore.add(ChatColor.GRAY + " За продажу еды...");
        fermerLore.add("");
        fermerLore.add(ChatColor.GRAY +"Специальный предмет: " +ChatColor.GOLD + "Золотое ялоко");
        fermerLore.add(ChatColor.GRAY + "Невозможно получить отрицательный бонус");
        fermerLore.add("");
        fermerLore.add(ChatColor.GOLD + "Нажмите, чтобы выбрать этот класс");
        Bukkit.getWorld(player.getWorld().getUID()).setDifficulty(Difficulty.EASY);
        fermerMeta.setLore(fermerLore);
        fermer.setItemMeta(fermerMeta);

        inventory.setItem(0, fermer);
        inventory.setItem(1, fermer);
        inventory.setItem(9, fermer);
        inventory.setItem(10, fermer);
        //KILLER
        ItemStack killer = new ItemStack(Material.BARRIER);
        ItemMeta killerMeta = gui.getItemMeta();
        killerMeta.setDisplayName(ChatColor.RED + "Класс Убийца");
        List<String> killerLore = new ArrayList<>();
        killerLore.add("");
        killerLore.add(ChatColor.GRAY + " Вы прирожденный убийца");
        killerLore.add(ChatColor.GRAY + " Зарабатывайте монеты");
        killerLore.add(ChatColor.GRAY + " За удар мобов и продажу лута!");
        killerLore.add("");
        killerLore.add(ChatColor.GRAY +"Пассивный эффект: " +ChatColor.DARK_RED + "Сила I");
        killerLore.add("");
        killerLore.add(ChatColor.GOLD + "Нажмите, чтобы выбрать этот класс");
        killerMeta.setLore(killerLore);
        killer.setItemMeta(killerMeta);

        inventory.setItem(3, killer);
        inventory.setItem(4, killer);
        inventory.setItem(5, killer);
        inventory.setItem(12, killer);
        inventory.setItem(13, killer);
        inventory.setItem(14, killer);
        //ШАхТЕР
        ItemStack waxter = new ItemStack(Material.BARRIER);
        ItemMeta waxterMeta = gui.getItemMeta();
        waxterMeta.setDisplayName(ChatColor.WHITE + "Класс ШАХТЕР");
        List<String> waxterLore = new ArrayList<>();
        waxterLore.add("");
        waxterLore.add(ChatColor.GRAY + " Вы прирожденный мамкин шахтер");
        waxterLore.add(ChatColor.GRAY + " Зарабатывайте монеты");
        waxterLore.add(ChatColor.GRAY + " За добычу и продажу ресурсов!");
        waxterLore.add("");
        waxterLore.add(ChatColor.GRAY +"Пассивный эффект: " +ChatColor.DARK_BLUE + "Сопротивление урону I");
        waxterLore.add("");
        waxterLore.add(ChatColor.GOLD + "Нажмите, чтобы выбрать этот класс");
        waxterMeta.setLore(waxterLore);
        waxter.setItemMeta(waxterMeta);

        inventory.setItem(7, waxter);
        inventory.setItem(8, waxter);
        inventory.setItem(16, waxter);
        inventory.setItem(17, waxter);
        //РЫБАК
        ItemStack pubak = new ItemStack(Material.BARRIER);
        ItemMeta pubakMeta = gui.getItemMeta();
        pubakMeta.setDisplayName(ChatColor.AQUA + "Класс Рыбак");
        List<String> pubakLore = new ArrayList<>();
        pubakLore.add("");
        pubakLore.add(ChatColor.GRAY + " Вы прирожденный мамкин Рыбак");
        pubakLore.add(ChatColor.GRAY + " Зарабатывайте монеты");
        pubakLore.add(ChatColor.GRAY + " За продажу рыбы и мусора!");
        pubakLore.add("");
        pubakLore.add(ChatColor.GRAY +"Пассивный эффект: " +ChatColor.RED + "Регенерация I");
        pubakLore.add("");
        pubakLore.add(ChatColor.GOLD + "Нажмите, чтобы выбрать этот класс");
        pubakMeta.setLore(pubakLore);
        pubak.setItemMeta(pubakMeta);

        inventory.setItem(29, pubak);
        inventory.setItem(30, pubak);
        inventory.setItem(38, pubak);
        inventory.setItem(39, pubak);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (checkOpenClass.get(player) == null) {
                    player.openInventory(inventory);
                    player.sendTitle("", "ВЫБЕРИТЕ КЛАСС", 0, 40, 0);
                }
            }
        }.runTaskTimer(plugin(),0,50);
    }
}
