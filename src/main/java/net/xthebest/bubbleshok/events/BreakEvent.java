package net.xthebest.bubbleshok.events;

import net.xthebest.bubbleshok.Main;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class BreakEvent implements Listener {
    public static Map<Material,Integer> blocksBreak = new HashMap<>();
    public static void addBlocks(){
        blocksBreak.put(Material.GRASS, 1);
        blocksBreak.put(Material.TALL_GRASS, 1);
        blocksBreak.put(Material.OAK_LEAVES, 1);
        blocksBreak.put(Material.BIRCH_LEAVES, 1);
        blocksBreak.put(Material.JUNGLE_LEAVES, 1);
        blocksBreak.put(Material.DARK_OAK_LEAVES, 1);
        blocksBreak.put(Material.SPRUCE_LEAVES, 1);
        blocksBreak.put(Material.DEAD_BUSH, 1);
        blocksBreak.put(Material.ACACIA_LEAVES, 1);
        blocksBreak.put(Material.MANGROVE_LEAVES, 1);

    }
    @EventHandler
    public void block(BlockBreakEvent event) {
        if(Main.gameStatus) {
            if(Main.status <= 1) {

                Block block = event.getBlock();
                Player player = event.getPlayer();
                if(Main.status == 0) {
                    for (Map.Entry<Material, Integer> entry : blocksBreak.entrySet()) {
                        if (block.getType().equals(entry.getKey())) {
                            block.setType(Material.AIR);
                            Random r = new Random();
                            int random = r.nextInt(6);
                            if (random == 5) {
                                Main.money.put(player, Main.money.get(player) + entry.getValue());
                            }
                        }
                    }
                }
                Material material = block.getType();

                if (Main.classFermer.get(player) != null) {
                    if (material.equals(Material.OAK_LEAVES)) {
                        Random r = new Random();
                        int random = r.nextInt(15);
                        if (random == 13) {
                            block.getWorld().dropItem(block.getLocation().add(0.5, 0.5, 0.5), new ItemStack(Material.APPLE));
                        }
                    }
                }
                if (Main.classWaxter.get(player) != null) {
                    if (material.equals(Material.COAL_ORE) || material.equals(Material.DEEPSLATE_COAL_ORE) || material.equals(Material.IRON_ORE) || material.equals(Material.DEEPSLATE_IRON_ORE) || material.equals(Material.GOLD_ORE) || material.equals(Material.DEEPSLATE_GOLD_ORE) || material.equals(Material.LAPIS_ORE) || material.equals(Material.DIAMOND_ORE) || material.equals(Material.DEEPSLATE_DIAMOND_ORE) || material.equals(Material.DEEPSLATE_EMERALD_ORE)) {
                        if (material.equals(Material.COAL_ORE) || material.equals(Material.DEEPSLATE_COAL_ORE)) {
                            Main.money.put(player, Main.money.get(player) + 2);
                        }
                        if (material.equals(Material.IRON_ORE) || material.equals(Material.DEEPSLATE_IRON_ORE)) {
                            Main.money.put(player, Main.money.get(player) + 5);
                        }
                        if (material.equals(Material.GOLD_ORE) || material.equals(Material.DEEPSLATE_GOLD_ORE)) {
                            Main.money.put(player, Main.money.get(player) + 15);
                        }
                        if (material.equals(Material.LAPIS_ORE)) {
                            Main.money.put(player, Main.money.get(player) + 2);
                        }
                        if (material.equals(Material.DIAMOND_ORE) || material.equals(Material.DEEPSLATE_DIAMOND_ORE)) {
                            Main.money.put(player, Main.money.get(player) + 10);
                        }
                        if (material.equals(Material.EMERALD_ORE) || material.equals(Material.DEEPSLATE_EMERALD_ORE)) {
                            Main.money.put(player, Main.money.get(player) + 7);
                        }
                    } else {
                        block.setType(Material.AIR);
                    }
                }
            }else{
                event.setCancelled(true);
            }
        }else{
            event.setCancelled(true);
        }
    }
}
